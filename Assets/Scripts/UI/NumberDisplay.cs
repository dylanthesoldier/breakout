﻿using Core;
using UnityEngine;
using UnityEngine.Serialization;

namespace UI
{
    /// <summary>
    /// The base class that holds the number and extended to create the specific functionality to "update display" specific code.
    /// </summary>
    /// <typeparam name="T">The type of display.</typeparam>
    public abstract class NumberDisplay<T> : MonoBehaviour
    {
        [SerializeField] private Number number;
        [SerializeField] private T display;
        
        public void Decrement(int amount)
        {
            number.Decrement(amount);
            _updateDisplay(display, number);
        }
        
        public int GetCurrentNumberAmount()
        {
            return number.GetAmount();
        }
        
        public void Increment(int amount)
        {
            number.Increment(amount);
            _updateDisplay(display, number);
        }
        
        public void ResetNumber()
        {
            number.Reset();
            _updateDisplay(display, number);
        }

        protected abstract void _updateDisplay(T displayObject, Number number);
    }
}