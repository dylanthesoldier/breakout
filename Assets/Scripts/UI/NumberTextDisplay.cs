using Core;
using TMPro;

namespace UI
{
    /// <summary>
    /// TextMeshPro Text Display implementation.
    /// </summary>
    public class NumberTextDisplay: NumberDisplay<TMP_Text>
    {
        protected override void _updateDisplay(TMP_Text displayObject, Number number)
        {
            if (displayObject != null)
                displayObject.text = number.GetAmount().ToString();
        }
    }
}