using UnityEngine;

namespace Core.Variables
{
    /// <summary>
    /// Base class for holding a variable in a scriptable object to inject through the inspector later. Extended classes wll specify type and menu entry attribute.
    /// </summary>
    /// <typeparam name="T">Type of variable</typeparam>
    public class Variable<T>: ScriptableObject
    {
        [SerializeField] private T variable;

        public T GetValue()
        {
            return variable;
        }
    }
}
