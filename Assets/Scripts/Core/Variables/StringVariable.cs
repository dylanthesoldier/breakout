using System;
using UnityEngine;

namespace Core.Variables
{
    /// <summary>
    /// String variable implementation.
    /// </summary>
    [CreateAssetMenu(fileName = "StringSO", menuName = "Variables/String Variable")]
    public class StringVariable : Variable<string> {}
}