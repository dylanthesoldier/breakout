﻿using UnityEngine;

namespace Core.Variables
{
    /// <summary>
    /// Float variable implementation.
    /// </summary>
    [CreateAssetMenu(fileName = "FloatSO", menuName = "Variables/Float Variable")]
    public class FloatVariable: Variable<float> {}
}