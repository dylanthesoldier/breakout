﻿using UnityEngine;

namespace Core
{
    /// <summary>
    /// Utility class with non specific functionality too small for individual classes.
    /// </summary>
    public static class Util
    {
        public static Vector2 GetRandomDirection(float boundsPosition, float exlusionArea)
        {
            float randomX;
            float triggerRange = Random.Range(-boundsPosition, boundsPosition);

            if (triggerRange >= 0)
            {
                randomX = Random.Range(boundsPosition, exlusionArea);
            }
            else
            {
                randomX = Random.Range(-boundsPosition, -exlusionArea);
            }
            
            return new Vector2(randomX, -2).normalized;
        }
        
        public static void ToggleSpriteRender(Renderer spriteRenderer)
        {
            spriteRenderer.enabled = !spriteRenderer.enabled;
        }
    }
}