﻿using UnityEngine;

namespace Core.Configurations
{
    /// <summary>
    /// Holds the configuration data for setting the paddle movement data.
    /// </summary>
    [CreateAssetMenu(menuName = "Configurations/Paddle Config", fileName = "PaddleConfig")]
    public class PaddleConfiguration : ScriptableObject
    {
        [SerializeField] private float bounds;
        [SerializeField] private float speed;
        [SerializeField, TextArea] private string description;

        public float GetSpeed()
        {
            return speed;
        }

        public float GetBounds()
        {
            return bounds;
        }
    }
}