﻿using UnityEngine;

namespace Core.Configurations
{
    /// <summary>
    /// Holds the configuration data for setting how the ball launches.
    /// </summary>
    [CreateAssetMenu(menuName = "Configurations/Ball Launch Config", fileName = "BallLaunchConfig")]
    public class BallLaunchConfiguration : ScriptableObject
    {
        [SerializeField] private float launchDelayInSeconds;
        [SerializeField] private float launchSpeed;
        [SerializeField] private float launchAngleTop;
        [SerializeField] private float launchAngleBottom;
        [SerializeField, TextArea] private string description;
        
        public float GetLaunchDelayInSeconds()
        {
            return launchDelayInSeconds;
        }

        public float GetLaunchSpeed()
        {
            return launchSpeed;
        }

        public float GetLaunchAngleTop()
        {
            return launchAngleTop;
        }

        public float GetLaunchAngleBottom()
        {
            return launchAngleBottom;
        }
    }
}