using UnityEngine;

namespace Core
{
    /// <summary>
    /// Base number class with the ability to increment, decrement, and reset.
    /// </summary>
    [System.Serializable]
    public class Number
    {
        [SerializeField] private int startingAmount;
        [SerializeField] private int trackedAmount;

        public void Increment(int amount)
        {
            trackedAmount += amount;
        }

        public void Decrement(int amount)
        {
            trackedAmount -= amount;
        }

        public void Reset()
        {
            trackedAmount = startingAmount;
        }

        public int GetAmount()
        {
            return trackedAmount;
        }

        public void SetAmount(int value)
        {
            trackedAmount = value;
        }
        
    }
}
