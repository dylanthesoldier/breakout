using Core;
using Core.Variables;
using UnityEngine;
using UnityEngine.Events;

namespace Events
{
    /// <summary>
    /// Launches an event upon collision with a matching tag.
    /// </summary>
    public class CollisionEventHandler : MonoBehaviour
    {
        public StringVariable gameObjectTag;
        public UnityEvent onTriggerExecute;
    
        private void OnCollisionEnter2D(Collision2D col)
        {
            if (col.gameObject.tag.Equals(gameObjectTag.GetValue()))
            {
                onTriggerExecute.Invoke();
            }
        }
    }
}
