using Core.Variables;
using UnityEngine;
using UnityEngine.Events;

namespace Events
{
    /// <summary>
    /// Launches an event when the object goes out of the bounds area.
    /// </summary>
    public class BoundsEventHandler : MonoBehaviour
    {
        [SerializeField] private float bounds;
        public UnityEvent executeOnOutOfBounds;
        
        private void Update()
        {
            if (_isOutOfBounds(bounds))
                _raiseEvent();
        }
    
        private bool _isOutOfBounds(float boundsPosition)
        {
            float xPos = transform.position.x;

            bool pastLeftBoundary = xPos < -boundsPosition;
            bool pastRightBoundary = xPos > boundsPosition;

            if (pastLeftBoundary || pastRightBoundary)
                return true;

            return false;
        }

        private void _raiseEvent()
        {
            executeOnOutOfBounds.Invoke();
        }
    }
}
