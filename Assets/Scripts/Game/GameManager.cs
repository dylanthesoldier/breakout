using System;
using Core.Variables;
using UI;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Manages all the blocks state, score, and lives.
    /// </summary>
    public class GameManager : MonoBehaviour
    {
        private GameObject[] _blocks;
        public StringVariable blockTag;
        private int _numberOfDestroyedBlocks;
        public NumberTextDisplay livesDisplay;
        public NumberTextDisplay scoreDisplay;

        private void Awake()
        {
            _blocks = _getBlocksByTagFromScene();
        }

        public void BlockBroken()
        {
            scoreDisplay.Increment(1);
            _numberOfDestroyedBlocks++;

            if (_allBlocksDestroyed())
            {
                _resetBlocks();
                _numberOfDestroyedBlocks = 0;
            }
                
        }
        public void LostLive()
        {
            livesDisplay.Decrement(1);
            bool allLivesGone = livesDisplay.GetCurrentNumberAmount() <= 0;
            
            if (allLivesGone)
                ResetGame();
        }
        public void ResetGame()
        {
            _resetScore();
            _resetLives();
            _resetBlocks();
        }

        private bool _allBlocksDestroyed()
        {
            if (_numberOfDestroyedBlocks >= _blocks.Length)
                return true; 
            
            return false;
        }
        private void _resetScore()
        {
            scoreDisplay.ResetNumber();
        }
        private void _resetLives()
        {
            livesDisplay.ResetNumber();
        }
        private void _resetBlocks()
        {
            foreach (GameObject block in _blocks)
                block.SetActive(true);
        }
        private GameObject[] _getBlocksByTagFromScene()
        {
            return GameObject.FindGameObjectsWithTag(blockTag.GetValue());
        }
        
        public void Test_DestroyAllBlocks()
        {
            foreach (GameObject block in _blocks)
            {
                block.SetActive(false);
                BlockBroken();
            }
            _resetBlocks();
        }
    }
}
