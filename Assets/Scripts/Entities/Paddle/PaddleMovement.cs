using System;
using Core;
using Core.Configurations;
using Core.Variables;
using UnityEngine;

namespace Entities.Paddle
{
    /// <summary>
    /// Gives the paddle movement and a bounds restriction.
    /// </summary>
    public class PaddleMovement : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D paddleRigidBody;
        [SerializeField] private PaddleConfiguration _paddleConfiguration;
        private float bounds;
        private float speed;
        private Vector2 _leftBoundsPosition;
        private Vector2 _rightBoundsPosition;

        private void Awake()
        {
            bounds = _paddleConfiguration.GetBounds();
            speed = _paddleConfiguration.GetSpeed();
        }

        private void Start()
        {
            float yPos = transform.position.y;
            _leftBoundsPosition = new Vector2(-bounds, yPos);
            _rightBoundsPosition = new Vector2(bounds, yPos);
        }
        private void Update()
        {
            float axisInput = Input.GetAxis("Horizontal");

            _restrictLeftBounds();
            _restrictRightBounds();
        
            paddleRigidBody.velocity = Vector2.right * axisInput * speed;
        }

        private void _restrictLeftBounds()
        {
            if (transform.position.x < -bounds)
                transform.position = _leftBoundsPosition;
        }
        private void _restrictRightBounds()
        {
            if (transform.position.x > bounds)
                transform.position = _rightBoundsPosition;
        }
    }
}
