using System.Collections;
using Core;
using Core.Variables;
using UnityEngine;
using UnityEngine.Events;

namespace Entities.Block
{
    /// <summary>
    /// Adds the ability to break blocks and launch other events upon collisions.
    /// </summary>
    public class BlockBreaker : MonoBehaviour
    {
        public StringVariable blockTag;
        public float breakDelayInSeconds = .1f;
        public UnityEvent blockCollisionEvent;
    
        private void OnCollisionEnter2D(Collision2D col)
        {
            if (!col.gameObject.tag.Equals(blockTag.GetValue())) 
                return;
            
            StartCoroutine(_disableGameObjectAndLaunchEventCO(col.gameObject));
        }
        private void _invokeCollisionEvent()
        {
            blockCollisionEvent.Invoke();
        }
    
        private IEnumerator _disableGameObjectAndLaunchEventCO(GameObject block)
        {
            SpriteRenderer spriteRenderer = block.GetComponent<SpriteRenderer>();
            Util.ToggleSpriteRender(spriteRenderer);
            yield return new WaitForSeconds(breakDelayInSeconds);
            block.SetActive(false);
            Util.ToggleSpriteRender(spriteRenderer);
            
            _invokeCollisionEvent();
        }
    }
}
