using System;
using System.Collections;
using UnityEngine;

namespace Entities.Ball
{
    /// <summary>
    /// Keep the ball at a certain speed range, and angle. Angle is a WIP.
    /// </summary>
    public class BallModulator : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D _rigidbody;
        [SerializeField] private float maxSpeedLimit;
        [SerializeField] private float minSpeedLimit;
        [SerializeField] private float minAngle;
        private Vector3 _lastPosition;

        public float MaxSpeedLimit
        {
            get => maxSpeedLimit;
            set => maxSpeedLimit = value;
        }
        public float MinSpeedLimit
        {
            get => minSpeedLimit;
            set => minSpeedLimit = value;
        }
        public float MinAngle
        {
            get => minAngle;
            set => minAngle = value;
        }

        private void Awake()
        {
            _lastPosition = transform.position;
        }
        private void FixedUpdate()
        {
            _handleTopSpeed();
            _handleMinSpeed();
            _handleMinAngle();
            _updateLocalVariables();
        }

        private void _handleTopSpeed()
        {
            bool overSpeedLimit = _rigidbody.velocity.magnitude > MaxSpeedLimit;
            if (overSpeedLimit)
            {
                _rigidbody.velocity = _rigidbody.velocity.normalized * MaxSpeedLimit;
            }
        }
        private void _handleMinSpeed()
        {
            bool overSpeedLimit = _rigidbody.velocity.magnitude < MinSpeedLimit;
            if (overSpeedLimit)
            {
                _rigidbody.velocity = _rigidbody.velocity.normalized * MinSpeedLimit;
            }
        }
        private void _handleMinAngle()
        {
            Vector2 velocity = _rigidbody.velocity;
            Vector2 direction = transform.position - _lastPosition;
            float currentAngle = Vector3.Angle(Vector2.right, direction);
            bool directionIsLeft = velocity.x < 0;
            bool directionIsRight = velocity.x > 0;
            
            if(currentAngle == 0) return;
            if (directionIsLeft)
            {
                currentAngle = (180 - currentAngle);
                bool outsideAngleLeftSide = currentAngle < MinAngle;
                if(outsideAngleLeftSide)
                    _rigidbody.velocity = new Vector2(-MinAngle, velocity.y);
            }
            if (directionIsRight)
            {
                bool outsideAngleRightSide = currentAngle < MinAngle; // closer to 0 then closer the closer to horizontal movement
                if(outsideAngleRightSide)
                    _rigidbody.velocity = new Vector2(MinAngle, velocity.y);
            }
            StartCoroutine(displayMsg(currentAngle.ToString(), 1f));
        }
        private void _updateLocalVariables()
        {
            _lastPosition = transform.position;
        }

        private IEnumerator displayMsg(string msg, float delayInSeconds)
        {
            yield return new WaitForSeconds(delayInSeconds); 
            Debug.Log(msg);
        }
    }
}
