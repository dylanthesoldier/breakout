using System.Collections;
using Core;
using Core.Configurations;
using UnityEngine;

namespace Entities.Ball
{
    /// <summary>
    /// Launches the ball based on the provided configuration. Stops and resets the ball.
    /// </summary>
    public class BallLauncher : MonoBehaviour
    {
        public Rigidbody2D ballRigidbody2D;
        public BallLaunchConfiguration launchConfiguration;

        private Ball _ball;

        private void Start()
        {
            _ball = new Ball(ballRigidbody2D, transform, Vector2.zero);
            ResetBall();
        }

        public void ResetBall()
        {
            _ball.StopMovement();
            _ball.ResetPosition();
            Vector2 randomDirection = _getRandomDirection() * launchConfiguration.GetLaunchSpeed();
            StartCoroutine(_ball.LaunchCO(randomDirection, launchConfiguration.GetLaunchDelayInSeconds()));
        }

        private Vector2 _getRandomDirection()
        {
            return Util.GetRandomDirection(launchConfiguration.GetLaunchAngleTop(), launchConfiguration.GetLaunchAngleBottom());
        }
    }
    
    internal class Ball
    {
        private readonly Rigidbody2D _rigidbody2D;
        private readonly Transform _transform;
        private readonly Vector2 _startingPosition;
        
        public Ball(Rigidbody2D rigidbody2D, Transform transform, Vector2 startingPosition)
        {
            _rigidbody2D = rigidbody2D;
            _startingPosition = startingPosition;
            _transform = transform;
        }
        
        public void ResetPosition()
        {
            _transform.position = _startingPosition;
        }
        public IEnumerator LaunchCO(Vector2 direction, float delayInSeconds)
        {
            yield return new WaitForSeconds(delayInSeconds);
            _rigidbody2D.velocity = direction;
        }
        public void StopMovement()
        {
            _rigidbody2D.velocity = Vector2.zero;
        }
    }
}

