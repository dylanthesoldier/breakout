using UnityEngine;
using UnityEngine.Events;

namespace Testing
{
    /// <summary>
    /// Launches an event when a key is pressed.
    /// </summary>
    public class KeyEventHandler : MonoBehaviour
    {
        public KeyCode keyCode;
        public UnityEvent executeOnKeyPressed;
        [TextArea] public string description;
        
        private void Update()
        {
            if (Input.GetKeyDown(keyCode))
            {
                executeOnKeyPressed.Invoke();
            }
        }
    }
}
